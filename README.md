# ddns-gd-bash-update

A little Bash script to updated a Dynamic DNS entry with Google Domains

## Usage

### Paramaters

1. hostname
2. username for hostname
3. password for hostname

### Example

```bash
./ddns.sh foo.bar.com username password
```

### Output

Outputs the response from Google Domain's API.

See the [Using the API to update your Dynamic DNS record](https://support.google.com/domains/answer/6147083?hl=en) section in Google Domain's documentation for possible responses.

### Compatibility

This works on Ubuntu 14.04 and 15.10 and OS X Yosemite right out of the box. It doesn't depend on much, and should work in most \*unix environments.

### Dependencies

cURL and Bash
