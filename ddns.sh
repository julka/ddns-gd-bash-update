#!/bin/bash

#
# ddns-gd-bash-update v1.0.0
# Copyright (c) 2016 Julka Grodel https://gitlab.com/julka/ddns-gd-bash-update
# License: MIT
#

hostname=$1
username=$2
password=$3

agent='Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36'
ip=`curl -s http://ipinfo.io/ip`
url='https://domains.google.com/nic/update'
data='hostname='$hostname'&myip='$ip
status=`curl -s -u $username:$password $url?$data`
echo $status
